/*
  Задание 1
  
 Напишите функцию, которая возвращает нечетные значения массива.
 [1,2,3,4] => [1,3]
 */
export const getOddValues = (arr) => arr.filter((elem) => elem % 2 != 0);




/*
 Задание 2
  
  Напишите функцию, которая возвращает наименьшее значение массива
  [1,2,3,4] => 1
 */
export const getMinValue = arr => Math.min(...arr);





/*
  Задание 3
  
  Напишите функцию, которая возвращает массив наименьших значение строк двумерного массива
  [
    [1,2,3,4],
    [1,2,3,4],
    [1,2,3,4],
    [1,2,3,4],
  ] 
  => [1,1,1,1]
 
  Подсказка: вложенные for
 */
export const getMinValuesFromRows = arr => {

  const aa = [];
  for (let i = 0; i < arr.length; i++){
    let min = arr[i][0];
    for (let j = 0; j < arr[i].length; j++){
      
      if (min > arr[i][j]) min = arr[i][j];
    }
    aa.push(min);

  }
  return aa;

}


/*
  Задание 4
  
  Напишите функцию, которая возвращает 2 наименьших значение массива
  [4,3,2,1] => [1,2]
 
  Подсказка: sort
 */
export const get2MinValues = arr => {
  const ai = arr.sort(function (a, b) {
    return a - b;
  });

  const rez = ai.slice(0, 2);
  return rez;
 
 
  

};


/*
  Задание 5
  
  Напишите функцию, которая возвращает количество гласных в строке
  ( a, e, i, o, u ).
  
  'Return the number (count) of vowels in the given string.' => 15
  
  Подсказка: indexOf/includes или (reduce, indexOf/includes) или (filter, indexOf/includes)
 */
export const getCountOfVowels = str => {
  // // let str = "Return the number (count) of vowels in the given string.";
  const vowels = ['a', 'e', 'i', 'o', 'u'];
  let counter = 0;
  for (let i = 0; i < str.length; i++) {
    for (let j = 0; j < vowels.length; j++) {
      if (str[i] === vowels[j]) counter++;
    }
  }
  return counter;
}



 /*
  Задание 6
  
  Реализовать функцию, на входе которой массив чисел, на выходе массив уникальных значений
  [1, 2, 2, 4, 5, 5] => [1, 2, 4, 5]
  
  Подсказка: reduce
  */
export const getUniqueValues = str => {
  //   const arr = [1, 2, 2, 4, 5, 5];
  //   let ne = [];
  //   ne.push(str[0]);
  //   for (let i = 0; i < str.length; i++) {
  //     for (let j = 0; j < ne.length; j++) {
  //       if (str[i]!= ne[j]) ne.push(str[i]);

  //     }
    
    
  //   }
  //   return ne;
  // }

  return str.reduce((total, elem) => {
    if (total.indexOf(elem) < 0) {
      total.push(elem);
    }
    return total;
  }, []);
}



 /*
  Задание 7
  
  Реализовать функцию, на входе которой массив строк, на выходе массив с длинами этих строк
   ['Есть', 'жизнь', 'на', 'Марсе'] => [4, 5, 2, 5]
  
  Подсказка: map
  */
export const getStringLengths = (arr) => arr.map((element) => element.length);


 /*
  Задание 8
  
  Напишите функцию, которая принимает на вход данные из корзины в следующем виде:
   [
       { price: 10, count: 2},
       { price: 100, count: 1},
       { price: 2, count: 5},
       { price: 15, count: 6},
   ]
  где price это цена товара, а count количество. Функция должна вернуть 
  итоговую сумму по данному заказу.
  */
export const getTotal = cartData => {
  return cartData.reduce((acc, { price, count }) => acc + price * count, 0);
};


 /*
 Задание 9
  
  Реализовать функцию, на входе которой число с ошибкой, на выходе строка с сообщением
  500 => Ошибка сервера
  401 => Ошибка авторизации
  402 => Ошибка сервера
  403 => Доступ запрещен
  404 => Не найдено
  ХХХ => '' (для остальных - пустая строка)
  */
export const getError = errorCode => {
  var foo = errorCode;
  switch (foo) {
    case 500:
      return "Ошибка сервера";
      // break;
    case 401: // foo равно 0, случай соответствует выражению и эти инструкции будут выполнены
      return "Ошибка авторизации";
      // break;
    // ПРИМЕЧАНИЕ: здесь могла находиться забытая инструкция break
    case 402: // В случае 'case 0:' не было break, инструкции данного случая также будут выполнены
      return "Ошибка сервера";
      //break; // В конце расположен break, поэтому выполнение не перейдёт к случаю 'case 2:'
    case 403:
      return "Доступ запрещен";
      //break;
    case 404:
      return "Не найдено";
      //break;
    default:
      return "";
  }


};


 
  // Задание 10
  
  // // Реализовать функцию, на входе которой объект следующего вида:
  // {
  //   firstName: 'Петр',
  //   secondName: 'Васильев',
  //   patronymic: 'Иванович'
  // }
  // на выходе строка с сообщением 'ФИО: Петр Иванович Васильев'
  
export const getFullName = user => {
  // user = {
  //   firstName: 'Петр',
  //   secondName: 'Васильев',
  //   patronymic: 'Иванович'
  // };
  // console.log("this is = " + [user]);
  let fullname = Object.values(user);
  return "ФИО: " + fullname[0] + " " + fullname[2] + " " +fullname[1];
}
  // 'ФИО: ${user.firstName ${user.patronymic} ${user.secondName}';


 /*
  Задание 11
  
  Реализовать функцию, которая принимает на вход 2 аргумента: массив чисел и множитель,
  а возвращает массив исходный массив, каждый элемент которого был умножен на множитель:
  
  [1,2,3,4], 5 => [5,10,15,20]
  */
export const getNewArray = (arr, mul) => arr.map((e) => e * mul);


 /*
  Задание 12
  
  Реализовать функцию, которая принимает на вход 2 аргумента: массив и франшизу,
  а возвращает строку с именнами героев разделенных запятой:
  
  [
     {name: “Batman”, franchise: “DC”},
     {name: “Ironman”, franchise: “Marvel”},
     {name: “Thor”, franchise: “Marvel”},
     {name: “Superman”, franchise: “DC”}
  ],
  Marvel
  => Ironman, Thor
  */
export const getHeroes = (heroes, franchise) => {
  let stroka = [];
  // let search = 'DC';
  var heroes_DC;
  for(var i = 0; i < heroes.length; i++) {
    if(heroes[i].franchise == franchise) {
      // stroka += heroes[i].name + ', '; 
      stroka.push(heroes[i].name);
    }
  
  }
  // let rez = stroka.substring(stroka.length - 1);
  return stroka.join(', ');

    
 
}

